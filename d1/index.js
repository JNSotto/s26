/*
	Client-Server Architecture
		- It is a computing model wherein a server hosts, delivers and mages resources that a client consumes.

		Client is an application which creates requests from a server. A client will trigger an action and response from the server. 

		Server is able to host and deliver resources requested by the client. A server can answer to multiple clients. 


		Node.js
			- Node.js was created because JavaScript was originally created and conceptualized to be used for front-end development. 
			- Node.js is an open source environment which allows to create backend applications using JS or JavaScript.

		Why is Node.js popular?
			Performance - Node.js is one of the most performing environment for creating backend application for JS

			NPM (Node Package Manager) - it is ont of the largest registry for packages. Packages are methods, functions and code that greatly helps or adds with applications. 

			Familliarity - JS is one of the most popular programming languages and Node.js uses JS as its primary language
 */

let http = require("http");
//We use the "require" directive to load Node.js modules 
//A module is a software compent or part of a program that contains one or more routines. 
//HTTP is a protocol that allows the fetching of resources such as HTML documents


http.createServer(function(request, response){
//createServer that listen a server in a specific port


// Use the writeHead() method to: 
	//Set a status conde for the response - a 200 means OK
	//Set sdthe content - type of the response as plain text message
	response.writeHead(200,{'Content-Type': 'text/plain'});
	
	//This sends the response with the the text content 'Hello World'
	response.end('Hello World');

}).listen(4000)
//A port is vitual point where network connections start and end 
// The server will be assigned to port '4000' via the listen(4000) method where the server will listen to any requests that are sent to it enventually communicating with our server.  

//When server is running, console will print the message.fd
console.log('Server Running at localhost:4000');

//REMINDER: make sure that before running the server in the ternimal, you must be inside file of your node.js where you want to connect
//ctrl-c : to stop the server
//npx kill-port 4000: to kill the server or force to close the server 