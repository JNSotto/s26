//Routing is the way which we handle our clients request based on their endpoints

const http = require('http');

const PORT = 4000;

const server = http.createServer((req, res) =>{

	if(request.url == '/hello'){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Hello World')		
	} else {

		// This will be our response if an endpoint passed in the client's request is not recognized or there is no designated route for this endpoint. 
		response.writeHead(404,{'Content-Type':'text/plain'});
		response.end('Page not Available')

	}

})
server.listen(PORT);

console.log(`Server is running at localhost:${PORT}.`);


